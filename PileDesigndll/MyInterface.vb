﻿Public Module MyInterface
    Public csvFn As String
    Public GSAFn As String
    Public newGSA As LinkGSA
    Public Const curGSAversion = GSA_version.v8_7

    Public Class Linker
        Public FunctionType As FunType
        Enum FunType
            TowerPileDesign_TriangleGrid
            TowerPileDesign_FreeStyle
            TowerPileDesign_Priority
            PodiumPileDesign_TriangleGrid
            Update2GSA
            getGSANode
            SetPileAndUpdateRection
        End Enum
        Sub New(csvPath As String, GSAPath As String, FT As FunType, Optional AddPara As String = "")
            csvFn = csvPath
            GSAFn = GSAPath
            Me.FunctionType = FT
            LinkUI(AddPara)
        End Sub

        Public Sub LinkUI(Optional Para1 As String = "")
            ProcessForm = New Form1
            ProcessForm.Show()
            ProcessForm.Top = True
            ProcessForm.LabelS.Text = "Linked and get csv and GSA."

            newGSA = New LinkGSA(GSAFn, curGSAversion)
            If newGSA.isGoOn Then
                Select Case FunctionType
                    Case FunType.TowerPileDesign_TriangleGrid
                        DesignCompresionPile4Tower_TriangleGrid()
                    Case FunType.TowerPileDesign_FreeStyle
                        DesignPile4Tower_FreeStyle()
                    Case FunType.TowerPileDesign_Priority
                        DesignPile_PriorityFisrt()
                    Case FunType.PodiumPileDesign_TriangleGrid
                        DesignTensoinPile4Podium_TriangleGrid()
                    Case FunType.Update2GSA
                        Updata2GSA(True)
                    Case FunType.getGSANode
                        ProcessForm.LabelS.Text = "Geting nodes from gsa raft."
                        GetNodeLocations(Para1, csvFn, GSAFn)
                    Case FunType.SetPileAndUpdateRection
                        SetPileAndUpdataRection()
                End Select
                newGSA.ReleasGSA(True)
                ProcessForm.LabelS.Text = outMessage_P & vbCrLf & "GSA analysis done."
                ProcessForm.LabelS.Text &= vbCrLf & "Process done, please close the form."
            Else
                ProcessForm.Close()
                ProcessForm.LabelS.Text &= vbCrLf & "Some probleam appeard."
            End If
            MsgBox("程序结束.")
        End Sub

    End Class
End Module
