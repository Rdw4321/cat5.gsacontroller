﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.LabelS = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LabelS
        '
        Me.LabelS.AutoSize = True
        Me.LabelS.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.LabelS.Location = New System.Drawing.Point(12, 9)
        Me.LabelS.Name = "LabelS"
        Me.LabelS.Size = New System.Drawing.Size(40, 13)
        Me.LabelS.TabIndex = 0
        Me.LabelS.Text = "Status:"
        '
        'Form1
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(508, 124)
        Me.Controls.Add(Me.LabelS)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "Message box"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelS As System.Windows.Forms.Label

    Private Sub LabelS_TextChanged(sender As Object, e As EventArgs) Handles LabelS.TextChanged
        Me.Refresh()
    End Sub
End Class
