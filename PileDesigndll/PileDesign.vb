﻿Public Module PubPare
    Public CF As New CadFunctions
    Public EF As New ExcelFunctions
    Public MF As New Myfunction
    Public callee As New CalleeProb
    Public listAllPile As PileList

    Public pileReNum As Int16

    Public Const iteration As Int16 = 2

    Public pileKz = 150000.0# 'N/mm
    Public pileCapacity As Double = -2400  'Unit is kN must be minus
    Public pileCapacityArray = {-9000.0#}

    Public useRatio = 0.85
    Public ProcessForm As PileDesigndll.Form1
    Public outMessage_P As String

    Public GSAAnalysisCount As Int16

    Public Const processFilePath As String = "C:\RDW\DWWorkFolder\jobs\Job43_HZHL\03_Pile Design\TowerPileDesign\T2 Pile Design Priority\processInfo.txt"
    Public Const is2Csv As Boolean = True

    Public totalPileForce, totalPileForce_error As Double
End Module

Public Module DesignProcess
    Public Sub SetPileAndUpdataRection()
        If newGSA.isGoOn Then
            ReadcsvfileAndGenPiles()
            updateReaction(isRun:=True, isSaveModel:=True)
            ShowProgress("Main done.")
        End If
    End Sub

    Public Sub DesignTensoinPile4Podium_TriangleGrid()
        If newGSA.isGoOn Then
            ReadcsvfileAndGenPiles()
            updateReaction(True)
            UnsetPileOverNecessaryNumber(2000)
            UnsetPileOverNecessaryNumber(1500)
            UnsetPileOverNecessaryNumber(1000)
            UnsetPileOverNecessaryNumber(500)
            UnsetPileOverNecessaryNumber()
            ResetPile2HelpOverLoadPile()
            WriteResult2Csv()
        End If
        outMessage_P = "Main done."
        Debug.Print(outMessage_P)
    End Sub

    Public Sub DesignCompresionPile4Tower_TriangleGrid()
        For Each TenCapa In pileCapacityArray

            pileCapacity = TenCapa
            Dim i As Int16
            GSAAnalysisCount = 0

            ReadcsvfileAndGenPiles(True)
            outMessage_P = ""

            pileKz *= 100
            updateReaction(True)
            pileKz /= 10
            UnsetPileOverNecessaryNumber(150)

            For i = 135 To 15 Step -15
                UnsetPileOverNecessaryNumber(i)
            Next i

            pileKz /= 10
            UnsetPileOverNecessaryNumber()

            'PileArrAlgorithm_1()
            'newGSA.ReleasGSA(True)
            'WriteResult2Csv()

            outMessage_P = "Main done."
            Debug.Print(outMessage_P)
        Next
    End Sub

    Sub DesignPile4Tower_FreeStyle()
        initialProcess()
        UnsetPileInAdjacentArea()
    End Sub

    Sub DesignPile_PriorityFisrt()
        Dim isShowMsg As Boolean = True
        initialProcess()
        ShowProgress(, isShowMsg)
        SetPriorityPiles()
        UnsetPileOverNecessaryNumber(500)
        ShowProgress(, isShowMsg)
        UnsetPileInAdjacentArea(100)
        ShowProgress(, isShowMsg)
        UnsetPileInAdjacentArea()
        ShowProgress(, isShowMsg)
        clearindeterminedList()
        'ResetPile2HelpOverLoadPile()
    End Sub
End Module

Public Module PileDesignProcessElement
    Public Sub ShowProgress(Optional outMessage As String = "", Optional isShowMsg As Boolean = False)
        Dim outM As String
        If outMessage = "" Then
            outM = outMessage_P
        Else
            outM = outMessage
        End If

        ProcessForm.LabelS.Text = outM
        If isShowMsg Then
            MsgBox(outM)
        End If
    End Sub

    Sub initialProcess()
        pileCapacity = pileCapacityArray(0)
        GSAAnalysisCount = 0
        ReadcsvfileAndGenPiles(True)
        outMessage_P = ""
        updateReaction(True)
    End Sub

    Public Sub Updata2GSA(Optional isRun As Boolean = False)
        'newGSA = New LinkGSA(GSAFn)
        If newGSA.isGoOn Then
            ReadcsvfileAndGenPiles()
            updateReaction(isRun)
            WriteResult2Csv()
        End If
    End Sub

    Public Sub ClearData()
        newGSA = New LinkGSA(GSAFn)
        If newGSA.isGoOn Then
            ReadcsvfileAndGenPiles()
            updateReaction(False)
            newGSA.ReleasGSA(False)
            'WriteResult2Txt_1()
        End If
        outMessage_P = "Main done."
        Debug.Print(outMessage_P)
    End Sub

    Public Sub DelRes()
        RedudeData(2)
        MsgBox("Reduce data done.")
    End Sub
    ''' <summary>
    ''' update GSA model, and get result.
    ''' </summary>s
    Sub updateReaction(Optional isRun As Boolean = True, Optional isSaveModel As Boolean = False)

        GSAAnalysisCount += 1

        outMessage_P &= "Start analyse:" & GSAAnalysisCount & "_Capacity:" & pileCapacity & "_" & DateAndTime.Now.ToString
        ProcessForm.LabelS.Text = outMessage_P
        outMessage_P = ""
        Debug.Print(outMessage_P)
        Dim t0 As DateTime = Now

        If isRun Then
            GSAobj.Delete("RESULTS")
            setPiles2GSA()

            outMessage_P &= "Start analyse:" & GSAAnalysisCount & "_Capacity:" & pileCapacity & "_" & DateAndTime.Now.ToString
            ProcessForm.LabelS.Text = outMessage_P
            GSAobj.Analyse()
            If isSaveModel Then
                GSAobj.Save()
            End If
        End If

        Dim t1 As DateTime = Now
        Dim elapsedTime As TimeSpan

        'MsgBox("please check the result.")
        getPileReactions()
        listAllPile.UpdateSubLists()
        WriteResult2Csv()

        outMessage_P &= "Pile requirement:" & CalPileRequirment() & vbCrLf
        outMessage_P &= "Pile set:" & listAllPile.setList.Count & vbCrLf
        outMessage_P &= "Pile indetermind:" & listAllPile.indeterminedList.Count & vbCrLf
        outMessage_P &= "Ave reaction:" & Int(listAllPile.GetAveReaction) & vbCrLf


        elapsedTime = t1.Subtract(t0)
        outMessage_P &= "Analysis take time : " & elapsedTime.TotalSeconds.ToString("0") & " seconds. " & vbCrLf
        ProcessForm.LabelS.Text = outMessage_P
        Debug.Print(outMessage_P)
    End Sub
    ''' <summary>
    ''' Read data from excel file.
    ''' </summary>
    Public Sub readExcelDataAndGenPileCollection()
        listAllPile = New PileList
        EF.LinkActiveSheet(,, "PileOpti")
        Dim i As Int16 = 15

        Do While EF.mySheet.Cells(i, 1).text <> ""
            Dim aPile As New Pile(EF.mySheet.Cells(i, 1).value, EF.mySheet.Cells(i, 2).value)
            listAllPile.Add(aPile)
            i += 1
        Loop
        Debug.Print("Get data from excel done.")
        'SortPileRelation()
        Debug.Print("Sort data from excel done.")
    End Sub
    ''' <summary>
    ''' Read data from txt file.
    ''' </summary>
    Public Sub setPiles2GSA()
        For Each aP As Pile In listAllPile
            aP.classifyPilAndSet()
        Next
    End Sub

    Public Sub getPileReactions()
        For Each aP As Pile In listAllPile
            If aP.status <> Pile.PileStatus.unSet Then aP.getReaction()
        Next
    End Sub

    Function CalPileRequirment()

        totalPileForce = CDbl(GSA_TotalModel.getReaction(1, "SUPPORT"))
        pileReNum = Math.Abs(Int(totalPileForce / (useRatio * pileCapacity)))
        Return pileReNum
    End Function

End Module
