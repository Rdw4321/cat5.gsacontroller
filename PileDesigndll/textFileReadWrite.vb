﻿Imports Microsoft.VisualBasic.FileIO

Module textFileReadandWrite
    Public TitleList As Array

    Public Class textLink
        Public fileReader As System.IO.StreamReader
        Sub New(Fn As String)
            fileReader = My.Computer.FileSystem.OpenTextFileReader(Fn)
        End Sub

        Public Sub release()
            fileReader.Close()
            fileReader = Nothing
        End Sub
    End Class

    Sub ReadcsvfileAndGenPiles(Optional isAllCompression As Boolean = False)

        listAllPile = New PileList
        Dim i As Long = 1

        Dim tfp As New TextFieldParser(csvFn) With
        {
        .Delimiters = New String() {","},
        .TextFieldType = FieldType.Delimited
        }

        TitleList = tfp.ReadFields()

        Do Until tfp.EndOfData = True
            Dim fields = tfp.ReadFields()
            If Not IsNothing(fields) Then
                Dim aPile As New Pile(,, fields)

                If isAllCompression Then
                    aPile.isCompression = True
                End If

                listAllPile.Add(aPile)
            End If
        Loop
        tfp.Close()

        For Each aPile In listAllPile
            aPile.getAdjacentPileObjbyAdjacentStr()
        Next

    End Sub
    Sub WriteResult2Csv(Optional isWrite As Boolean = True)
        If isWrite Then
            Dim lines() As String = System.IO.File.ReadAllLines(csvFn)

            lines(0) = "Name,Coordinate[m],TopLevel,BottomLevel,Type,PileType,GSA Node,Adjacent,Adjacent_far,Time,isSet,Reaction[kN],Design Process Feature,isCompression,Cap_ten[kN],Stiff_ten[kN/m],Cap_com[kN],Stiff_Com[kN/m]"
            For i = 1 To UBound(lines)
                lines(i) = listAllPile(i - 1).ToString_Pile
            Next

            Try
                System.IO.File.WriteAllLines(csvFn, lines)
            Catch ex As Exception
            Finally
            End Try
        End If

    End Sub

    Sub WriteProcessInfo(text As String, Optional processFile As String = "")
        If processFile <> "" Then
            System.IO.File.WriteAllText(processFile, text)
            MsgBox(text)
        End If
    End Sub


    Function UpdateData(Ostr As String, Seperator As String, Data As String, isReplace As Boolean) As String
        Dim temp As String()
        temp = Ostr.Split(Seperator)
        If isReplace Then
            temp(Len(temp) - 1) = Data
            Return Join(temp, Seperator)
        Else
            Return Ostr & Seperator & Data
        End If
    End Function

    Sub RedudeData(N As Int16)
        Dim lines() As String = System.IO.File.ReadAllLines(csvFn)
        For i = 0 To UBound(lines)
            Dim temp As String()
            temp = lines(i).Split(vbTab)
            Array.Resize(Of String)(temp, temp.Length - 2)
            lines(i) = Join(temp, vbTab)
        Next
        System.IO.File.WriteAllLines(csvFn, lines)
    End Sub

End Module
