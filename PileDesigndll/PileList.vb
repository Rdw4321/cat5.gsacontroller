﻿''' <summary>
''' All possible pile location is in the list.
''' </summary>
Public Class PileList
    Inherits List(Of Pile)

    Public setPileNumber As Long
    Public averageReaciton As Double
    Public setList, unsetList, indeterminedList, setList_noHelp As List(Of Pile)
    Public priorityList As List(Of Pile)

    Function GetAveReaction()
        averageReaciton = 0

        For Each aP In setList
            averageReaciton += aP.Reaction
        Next

        For Each aP In indeterminedList
            averageReaciton += aP.Reaction
        Next
        averageReaciton = averageReaciton / (setList.Count + indeterminedList.Count)
        Return averageReaciton
    End Function
    Function getTotalReaction()
        Dim totalReaction As Double = 0

        For Each ap In Me
            If ap.status <> Pile.PileStatus.unSet Then
                totalReaction += ap.Reaction
            End If
        Next

        Return totalReaction
    End Function

    Sub UpdateSubLists()
        setList = New List(Of Pile)
        setList_noHelp = New List(Of Pile)
        unsetList = New List(Of Pile)
        indeterminedList = New List(Of Pile)
        priorityList = New List(Of Pile)

        For Each ap As Pile In Me
            Select Case ap.status
                Case Pile.PileStatus.hasBeenSet
                    setList.Add(ap)
                Case Pile.PileStatus.hasBeenSet_noHelp
                    setList_noHelp.Add(ap)
                Case Pile.PileStatus.indetermined
                    If ap.DesignType = Pile.DType.priority Then
                        priorityList.Add(ap)
                    Else
                        indeterminedList.Add(ap)
                    End If
                Case Pile.PileStatus.unSet
                    unsetList.Add(ap)
            End Select
        Next
    End Sub

    Sub combineSubList()
        For Each ap As Pile In setList_noHelp
            setList.Add(ap)
        Next
        setList_noHelp = Nothing
    End Sub
End Class
