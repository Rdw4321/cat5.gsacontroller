﻿Imports System.Math
Module PileArrangementAlgorithm
    ''' <summary>
    ''' distribute force method
    ''' in the method 
    ''' </summary>
    Sub UnsetPileOverNecessaryNumber(Optional addtionalPile As Int16 = 0)
        listAllPile.UpdateSubLists()

        listAllPile.indeterminedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList

        Do While listAllPile.indeterminedList.Last.Reaction > useRatio * pileCapacity * 1000 And listAllPile.indeterminedList.Count + listAllPile.setList.Count > pileReNum + addtionalPile
            WriteProcessInfo(listAllPile.indeterminedList.Last.Name, True)

            listAllPile.indeterminedList.Last.UnsetPile_giveoutForcebyNumberIn3d(True)

            outMessage_P = String.Format("According to Necessary Number principle, unset pile {0}_PileNeed:{1}_PileAdd:{2}_PileNow{3}", {listAllPile.indeterminedList.Last.Name, pileReNum, addtionalPile, listAllPile.setList.Count})

            ProcessForm.LabelS.Text = outMessage_P

            listAllPile.UpdateSubLists()
            listAllPile.indeterminedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList
            'WriteResult2Csv()
        Loop

        updateReaction(True)
        WriteResult2Csv()
    End Sub



    ''' <summary>
    ''' Delete pile inside 3d area.
    ''' </summary>
    Sub UnsetPileInAdjacentArea(Optional addtionalPile As Int16 = 0)

        Dim SortedList As List(Of Pile)
        listAllPile.UpdateSubLists()
        SortedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList

        Dim i As Int16 = 0

        Do While listAllPile.setList.Count < pileReNum And listAllPile.indeterminedList.Count > 0 And listAllPile.setList.Count + listAllPile.indeterminedList.Count > pileReNum + addtionalPile

            If SortedList(0).GetSumReactionIn3d(True) + SortedList(0).Reaction <= useRatio * pileCapacity * 1000 Then
                outMessage_P = "According to force sort set pile " & SortedList(0).Name & " Force in 3d: " & Int(SortedList(0).SumForce3d / 1000)
                SortedList(0).SetPileAndUpdateAdjacnet3d()
                listAllPile.UpdateSubLists()
            Else
                SortedList.Reverse()
                outMessage_P = "Unset pile " & SortedList(0).Name
                SortedList(0).UnsetPile_giveoutForcebyNumberIn3d(True)
                listAllPile.UpdateSubLists()
            End If

            'WriteResult2Csv()
            'WriteProcessInfo(SortedList(0).Name, processFilePath)

            ProcessForm.LabelS.Text = outMessage_P
            SortedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList
        Loop

        'clearindeterminedList()
        WriteResult2Csv()

        updateReaction()
    End Sub

    Sub SetPriorityPiles()
        Dim SortedList As List(Of Pile)
        listAllPile.UpdateSubLists()
        SortedList = listAllPile.priorityList.OrderBy(Function(x) x.Reaction).ToList

        Do While listAllPile.priorityList.Count > 0
            SortedList(0).SetPileAndUpdateAdjacnet3d()
            listAllPile.UpdateSubLists()

            outMessage_P = "According to priority principle, set pile" & SortedList(0).Name

            'WriteResult2Csv()
            'WriteProcessInfo(SortedList(0).Name , processFilePath)

            ProcessForm.LabelS.Text = outMessage_P
            SortedList = listAllPile.priorityList.OrderBy(Function(x) x.Reaction).ToList
        Loop
        WriteResult2Csv()

    End Sub

    Public Sub clearindeterminedList()
        Dim SortedList As List(Of Pile)
        listAllPile.UpdateSubLists()
        SortedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList

        Do While listAllPile.indeterminedList.Count > 0 And listAllPile.setList.Count < pileReNum
            SortedList(0).SetPileAndUpdateAdjacnet3d()
            listAllPile.UpdateSubLists()
            SortedList = listAllPile.indeterminedList.OrderBy(Function(x) x.Reaction).ToList
        Loop

        For Each ap In listAllPile.indeterminedList
            ap.status = Pile.PileStatus.unSet
        Next
        updateReaction()
        WriteResult2Csv()
    End Sub

    ''' <summary>
    ''' add piles to help overloaded
    ''' </summary>
    Sub ResetPile2HelpOverLoadPile()
        listAllPile.UpdateSubLists()
        listAllPile.setList = listAllPile.setList.OrderBy(Function(x) x.Reaction).ToList

        Do While listAllPile.setList(0).Reaction < pileCapacity * 1000 * useRatio And listAllPile.setList.Count + listAllPile.setList_noHelp.Count < pileReNum
            listAllPile.setList(0).ResetHelpPile()

            listAllPile.UpdateSubLists()
            listAllPile.setList = listAllPile.setList.OrderBy(Function(x) x.Reaction).ToList
        Loop
        listAllPile.combineSubList()

        WriteResult2Csv()
        updateReaction(True)
    End Sub
End Module


