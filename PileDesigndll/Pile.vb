﻿
Imports System.Math
Public Class Pile
    Public Name As String
    Public Top, Bot As Double
    Public GSANode As GSA_Node
    Public RealLoc As String
    Public Reaction As Double
    ' Public isSet As Boolean = True, isEnemySet As Boolean
    Public status As PileStatus
    'Public enemyPiles, friendPiles As New Dictionary(Of Double, Pile)
    Public PileDesignTypeStr, Type As String
    Public DesignType As DType

    Public InSide3d_indetermindAndSet, InSide6d_indetermindAndSet As New List(Of Pile)
    Public InSide3d_canBack, InSide6d_canBack As New List(Of Pile)
    Public InSide3d_set, InSide6d_set As New List(Of Pile)

    Dim strInSide3d_origin, strInSide6d_origin As String
    Public SumForce3d, SumForce6d As Single

    Public restInAd, MaxReactionInAd As Single
    Public isCompression As Boolean
    Public capacity As Single, stiffness As Single ' default is tension
    Public capacity_com, stiffness_com, capacity_ten, stiffness_ten As Single

    Public Enum DType
        common = 1
        priority = 2
        fix = 4
        negelect = 0
        anotherIn3d = 8
    End Enum

    Public Enum PileStatus
        hasBeenSet = 1
        hasBeenSet_noHelp = 11
        unSet = 0
        indetermined = 2
        fix = 3
    End Enum

    Sub New(Optional aPileName As String = "", Optional aGSANodeNum As Long = 0, Optional fields As Array = Nothing,
            Optional isReadResult As Boolean = False, Optional isCom As Boolean = False)
        If IsNothing(fields) Then
            Me.Name = aPileName
            Me.GSANode = New GSA_Node(aGSANodeNum)
        Else
            Dim infoList = fields
            Name = GetValue(infoList, "Name", " ")
            RealLoc = GetValue(infoList, "Coordinate[m]", " ")
            Top = GetValue(infoList, "TopLevel", 0)
            Bot = GetValue(infoList, "BottomLevel", 0)
            Type = GetValue(infoList, "Type", " ")
            PileDesignTypeStr = GetValue(infoList, "PileType", " ")

            capacity_ten = CDec(GetValue(infoList, "Cap_ten[kN]", 0))
            capacity_com = CDec(GetValue(infoList, "Cap_com[kN]", 0))
            capacity = capacity_ten


            stiffness_ten = CDec(GetValue(infoList, "Stiff_ten[kN/m]", 0))
            stiffness_com = CDec(GetValue(infoList, "Stiff_Com[kN/m]", 0))
            stiffness = stiffness_ten

            Reaction = CDec(GetValue(infoList, "Reaction[kN]", 0))

            If PileDesignTypeStr.Contains("Ten") Then
                isCompression = False
                Reaction = -1
            Else
                isCompression = True
                Reaction = 1
            End If

            UpdateStiffnessAndCapacity()

            GSANode = New GSA_Node(CInt(GetValue(infoList, "GSA Node", 0)))

            status = PileStatus.indetermined

            If PileDesignTypeStr = "Priority" Then
                DesignType = DType.priority
            ElseIf PileDesignTypeStr.Contains("Fix") Then
                DesignType = DType.fix
                status = PileStatus.hasBeenSet
            Else
                DesignType = DType.common
            End If



            strInSide3d_origin = GetValue(infoList, "Adjacent", " ")
            strInSide6d_origin = GetValue(infoList, "Adjacent_far", " ")


            'Dim dIsSet As Int16 = GetValue(infoList, "isSet", "1")
            'If dIsSet = 1 Then isSet = True Else isSet = False

            'Reaction = GetValue(infoList, "Reaction", 0)
        End If

        If capacity_com Then
            isCompression = isCom
        End If

        outMessage_P = "New Pile info " & Name & " has been created." & String.Format("_{0}_{1}", {capacity, stiffness * 1000})
        Debug.Print(outMessage_P)
        ProcessForm.LabelS.Text = outMessage_P
    End Sub

    Sub getAdjacentPileObjbyAdjacentStr()
        Dim InSide3dArr As String()
        Dim InSide6dArr As String()

        InSide3dArr = strInSide3d_origin.Split(" ")

        For Each aPileStr In InSide3dArr
            If aPileStr <> "" Then InSide3d_indetermindAndSet.Add(listAllPile.Find(Function(p) p.Name = aPileStr))
        Next

        InSide6dArr = strInSide6d_origin.Split(" ")
        For Each aPileStr In InSide6dArr
            If aPileStr <> "" Then InSide6d_indetermindAndSet.Add(listAllPile.Find(Function(p) p.Name = aPileStr))
        Next
        ShowProgress(Name & " has get adjacent piles list.")

    End Sub

    Function GetValue(infoList As Array, Key As String, Def As String)
        Dim Loc As Int16 = Array.IndexOf(TitleList, Key)
        If Loc = -1 Then
            Return Def
        Else
            Return infoList(Loc)
        End If
    End Function

    Sub classifyPilAndSet()
        UpdateStiffnessAndCapacity()
        If DesignType = DType.negelect Then
            UpdataPile2GSA()
        ElseIf DesignType = DType.fix Then
            UpdataPile2GSA(True)
        Else
            If status <> PileStatus.unSet Then
                UpdataPile2GSA(True)
            Else
                UpdataPile2GSA()
            End If
        End If
    End Sub

    Sub UpdataPile2GSA(Optional isSet As Boolean = False)
        If isSet Then
            GSANode.updateNode(, {stiffness, stiffness, stiffness, 0#, 0#, 0#})
            ShowProgress(Name & " has been set to GSA. Capa: " & capacity & "Stiffness: " & stiffness)
        Else
            GSANode.updateNode(, {0#, 0#, 0#, 0#, 0#, 0#})
            ShowProgress(Name & " has been unset to GSA. Capa: " & capacity & "Stiffness: 0")
        End If
    End Sub

    Sub SetPile()
        'isSet = True
        status = PileStatus.hasBeenSet
        Reaction += 1.0E+20
    End Sub


    Sub SetPileAndUpdateAdjacnet3d(Optional isCom As Boolean = False)
        status = PileStatus.hasBeenSet
        updateAdjacentStatus()

        GetSumReactionIn3d(isCom)

        Dim forceAbsorb As Single = capacity - Reaction

        If forceAbsorb > SumForce3d Then
            Reaction = capacity
        Else
            Reaction += SumForce3d
        End If

        Dim SumForce3d_abs = GetSumReactionIn3d(True)

        For Each anAdjacentPile In InSide3d_indetermindAndSet
            anAdjacentPile.Reaction -= forceAbsorb * Math.Abs(anAdjacentPile.Reaction) / SumForce3d_abs
        Next

        'WriteProcessInfo(Name, processFilePath)
        'WriteResult2Csv(is2Csv)

        For Each adPile In InSide3d_indetermindAndSet
            If Not IsNothing(adPile) Then
                adPile.UnsetPile_giveoutForcebyNumberIn3d(True)
                adPile.DesignType = DType.anotherIn3d
            End If
        Next

        For Each adPile In InSide3d_canBack
            If Not IsNothing(adPile) Then
                adPile.DesignType = DType.anotherIn3d
            End If
        Next

    End Sub

    Function getReaction()
        MaxReactionInAd = 0
        If status <> PileStatus.unSet Then
            Dim temp = GSANode.getRactionZ("1")
            If IsNumeric(temp) Then
                Reaction = temp
            End If
        Else
            Reaction = 0
        End If

        ProcessForm.LabelS.Text = outMessage_P & vbCrLf & "Get reaction for pile " & Name & "."
        Return Reaction
    End Function
    Sub UnsetPile_giveoutForcebyNumberIn3d(Optional isTake6dIntoConsider As Boolean = False)
        SumForce3d = GetSumReactionIn3d(isCompression)
        'If IsMustSet() Then
        'Reaction = -100 * 1000
        'Else
        status = PileStatus.unSet
            updateAdjacentStatus()

            For Each aPile In InSide3d_indetermindAndSet
                If Not IsNothing(aPile) Then
                    aPile.Reaction += Reaction * Abs(aPile.Reaction) / InSide3d_indetermindAndSet.Count
                End If
            Next
        'End If
    End Sub
    Sub UnsetPile_giveoutForce_SS(Optional isTake6dIntoConsider As Boolean = False)
        SumForce3d = GetSumReactionIn3d(isCompression)
        SumForce6d = GetSumReactionIn6d(isCompression)

        If IsMustSet() Then
            Reaction = -100 * 1000
        Else
            status = PileStatus.unSet
            updateAdjacentStatus()

            For Each aPile In InSide3d_indetermindAndSet
                If Not IsNothing(aPile) Then
                    aPile.Reaction += Reaction * Abs(aPile.Reaction) / (SumForce3d + SumForce6d)
                End If
            Next

            If isTake6dIntoConsider = True Then
                For Each aPile_6d In InSide6d_indetermindAndSet
                    If Not IsNothing(aPile_6d) Then
                        aPile_6d.Reaction += Reaction * Abs(aPile_6d.Reaction) / (SumForce3d + SumForce6d)
                    End If
                Next
            End If

            'WriteProcessInfo(Name, processFilePath)
            'WriteResult2Csv(is2Csv)
        End If
    End Sub
    ''' <summary>
    ''' if the unset the pile, any pile in adjacent would surpass its capacity then the pile must be keep set.
    ''' </summary>
    Function IsMustSet()
        Dim isCom As Boolean
        If Reaction < 0 Then
            isCom = False
        Else
            isCom = True
        End If

        For Each aPile In InSide3d_indetermindAndSet
            If isCom Then
                If Not IsNothing(aPile) Then
                    If aPile.Reaction + Reaction * Abs(aPile.Reaction) / (SumForce3d + SumForce6d) > aPile.capacity_com * 1000 Then
                        Return True
                    End If
                End If
            Else
                If Not IsNothing(aPile) Then

                    If aPile.Reaction + Reaction * Abs(aPile.Reaction) / (SumForce3d + SumForce6d) < aPile.capacity_ten * -1000 Then
                        Return True
                    End If
                End If
            End If
        Next
        Return False
    End Function
    ''' <summary>
    ''' find a pile inside 3d area to help the pile, if no find in 6d
    ''' </summary>
    Sub ResetHelpPile()
        updateAdjacentStatus()

        For Each aPile In InSide6d_canBack
            aPile.SetPileAndAbsortFrom6d()
            Exit For
        Next
        status = PileStatus.hasBeenSet_noHelp

    End Sub

    Sub SetPileAndAbsortFrom6d()
        status = PileStatus.hasBeenSet
        For Each ap In InSide3d_canBack
            ap.DesignType = DType.anotherIn3d
        Next

        updateAdjacentStatus()
        Dim SumForce6d_abs = GetSumReactionIn6d(True)

        Dim forceAbsorb As Single = capacity - Reaction

        For Each anAdjacentPile In InSide6d_set
            anAdjacentPile.Reaction -= forceAbsorb * Math.Abs(anAdjacentPile.Reaction) / SumForce6d_abs
        Next

    End Sub

    Function GetSumReactionIn3d(isGetCom As Boolean) As Single 'com is +, ten is - 
        SumForce3d = 0
        For Each adPile In InSide3d_indetermindAndSet
            If Not IsNothing(adPile) Then
                If adPile.status = PileStatus.indetermined Then
                    If isGetCom Then
                        If adPile.Reaction > 0 Then
                            SumForce3d += adPile.Reaction
                        End If
                    Else
                        If adPile.Reaction < 0 Then
                            SumForce3d += adPile.Reaction
                        End If
                    End If

                End If
            End If
        Next
        Return SumForce3d
    End Function

    Function GetSumReactionIn6d(Optional isGetCom As Boolean = False) As Single 'com is +, ten is - 
        SumForce6d = 0

        For Each adPile In InSide6d_indetermindAndSet
            If Not IsNothing(adPile) Then
                If isGetCom Then
                    If adPile.Reaction > 0 Then
                        SumForce6d += adPile.Reaction
                    End If
                Else
                    If adPile.Reaction < 0 Then
                        SumForce6d += adPile.Reaction
                    End If
                End If
            End If
        Next

        Return SumForce6d
    End Function

    Sub updateAdjacentStatus()
        Dim tempList As Array
        tempList = InSide3d_indetermindAndSet.ToArray

        For Each adPile In tempList
            If Not IsNothing(adPile) Then
                If adPile.status = PileStatus.unSet Then
                    InSide3d_indetermindAndSet.Remove(adPile)
                End If

                If adPile.status = PileStatus.hasBeenSet Then
                    InSide3d_set.Add(adPile)
                ElseIf adPile.status = PileStatus.unSet And adPile.DesignType <> DType.anotherIn3d Then
                    InSide3d_canBack.Add(adPile)
                End If
            End If
        Next

        tempList = InSide6d_indetermindAndSet.ToArray
        For Each adPile In tempList
            If Not IsNothing(adPile) Then
                If adPile.status = PileStatus.unSet Then
                    InSide6d_indetermindAndSet.Remove(adPile)
                End If

                If adPile.status = PileStatus.hasBeenSet Then
                    InSide6d_set.Add(adPile)
                ElseIf adPile.status = PileStatus.unSet And adPile.DesignType <> DType.anotherIn3d Then
                    InSide6d_canBack.Add(adPile)
                End If
            End If
        Next

    End Sub

    Function ToString_Pile()

        If PileDesignTypeStr = "Fix" Then
            'Reaction -= 10000000000.0
        End If

        Dim Ret As String = String.Join(",", {Name, """" & RealLoc & """", Top, Bot, Type, PileDesignTypeStr, GSANode.ID, strInSide3d_origin,
                                        strInSide6d_origin, "-", status, Reaction, DesignType, isCompression, capacity_ten, stiffness_ten, capacity_com, stiffness_com})

        Return Ret
    End Function
    Sub UpdateStiffnessAndCapacity()
        If Reaction < 0 Then
            isCompression = False
        ElseIf Reaction > 0 Then
            isCompression = True
        End If

        If isCompression Then
            stiffness = stiffness_com / 1000
            capacity = capacity_com
        Else
            stiffness = stiffness_ten / 1000
            capacity = capacity_ten
        End If
    End Sub

End Class
