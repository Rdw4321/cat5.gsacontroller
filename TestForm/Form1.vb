﻿Public Class Form1
    Public GSAFileName, csvFN, csvFN1 As String
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        getData()
        Dim aLink As New Linker(csvFN, GSAFileName, Linker.FunType.TowerPileDesign_Priority)
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.Closing
        My.Settings.Save()
    End Sub


    Private Sub getData()
        GSAFileName = GSAFilePathTextBox.Text.Replace("""", "")
        csvFN = DesignResultCSVTextBox.Text.Replace("""", "")
        csvFN1 = TextBox1.Text.Replace("""", "")
        My.Settings.Save()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        getData()
        Dim aLink As New Linker(csvFN, GSAFileName, Linker.FunType.SetPileAndUpdateRection)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        getData()
        MyInterface.MyInterface.LinkUI(GSAFileName)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim MyFile As String, Parameters() As String
        Dim i As Int16
        Parameters = System.Environment.GetCommandLineArgs()
        i = Parameters.GetUpperBound(0)

        If i > 0 Then
            MyFile = Parameters(1)
        End If
        'MsgBox(MyFile)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        GSA_TotalModel.ShowBox = TextBox2
        GSA_TotalModel.ModelRunMain(Application.StartupPath & "\OptTest-JB.gwb")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        getData()
        Dim aLink As New Linker(csvFN, GSAFileName, Linker.FunType.getGSANode, csvFN1)

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        GSA_TotalModel.genOptSectionList(Application.StartupPath & "\OptTest-JB.gwb")
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        getData()
        Module_BuildModel.Build_Main(GSAFileName, csvFN, csvFN1)
        MsgBox("done.")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        getData()
        Dim aLink As New Linker(csvFN, GSAFileName, Linker.FunType.PodiumPileDesign_TriangleGrid)
    End Sub

End Class
