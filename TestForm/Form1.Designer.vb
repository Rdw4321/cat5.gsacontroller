﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GSAFilePathTextBox = New System.Windows.Forms.TextBox()
        Me.DesignResultCSVTextBox = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(185, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "TowerPileDesign"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 100)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "GSA File"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 155)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Input CSV "
        '
        'GSAFilePathTextBox
        '
        Me.GSAFilePathTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.TestForm.My.MySettings.Default, "GSAFilePath", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.GSAFilePathTextBox.Location = New System.Drawing.Point(90, 100)
        Me.GSAFilePathTextBox.Multiline = True
        Me.GSAFilePathTextBox.Name = "GSAFilePathTextBox"
        Me.GSAFilePathTextBox.Size = New System.Drawing.Size(462, 49)
        Me.GSAFilePathTextBox.TabIndex = 1
        Me.GSAFilePathTextBox.Text = Global.TestForm.My.MySettings.Default.GSAFilePath
        '
        'DesignResultCSVTextBox
        '
        Me.DesignResultCSVTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.TestForm.My.MySettings.Default, "DesignResultCSVPath", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.DesignResultCSVTextBox.Location = New System.Drawing.Point(90, 155)
        Me.DesignResultCSVTextBox.Multiline = True
        Me.DesignResultCSVTextBox.Name = "DesignResultCSVTextBox"
        Me.DesignResultCSVTextBox.Size = New System.Drawing.Size(462, 49)
        Me.DesignResultCSVTextBox.TabIndex = 1
        Me.DesignResultCSVTextBox.Text = Global.TestForm.My.MySettings.Default.DesignResultCSVPath
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(15, 41)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(185, 23)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "Podium Pile Design"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.Location = New System.Drawing.Point(12, 286)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(185, 23)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Build Nodes"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.TestForm.My.MySettings.Default, "CSVFile1", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.TextBox1.Location = New System.Drawing.Point(90, 210)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(462, 49)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = Global.TestForm.My.MySettings.Default.CSVFile1
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button4.Location = New System.Drawing.Point(12, 328)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(185, 23)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Just Get Rection"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button5.Location = New System.Drawing.Point(12, 386)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(185, 23)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "ElementOpt"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button6.Location = New System.Drawing.Point(247, 386)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(185, 23)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "ElementOptw/Hst"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button7.Location = New System.Drawing.Point(217, 41)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(185, 23)
        Me.Button7.TabIndex = 0
        Me.Button7.Text = "GetGSANode"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 210)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Special Para"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(708, 97)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(231, 52)
        Me.TextBox2.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(662, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Status:"
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button8.Location = New System.Drawing.Point(456, 386)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(185, 23)
        Me.Button8.TabIndex = 0
        Me.Button8.Text = "生成dwIO"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(998, 474)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.DesignResultCSVTextBox)
        Me.Controls.Add(Me.GSAFilePathTextBox)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents GSAFilePathTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DesignResultCSVTextBox As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Button8 As Button
End Class
