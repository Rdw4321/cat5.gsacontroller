﻿using ArmadilloCsharp;
using Grasshopper.Kernel;

namespace GHComponent_AutoPile
{
    public class Cats5GrasshopperStartTracking : GH_AssemblyPriority
    {
        internal static bool CanRun = false;
        internal static string AppName = "Auto Pile Design";

        public override GH_LoadingInstruction PriorityLoad()
        {
            string appVersion = "1.0";
            if (Licence.CheckLicence(AppName, appVersion, true))
            {
                CanRun = true;
                TrackingInfo trackInfo = new TrackingInfo(AppName, "GH_Loading");
                bool logResult = trackInfo.StartFeature();
            }
            return GH_LoadingInstruction.Proceed;
        }
    }
}
