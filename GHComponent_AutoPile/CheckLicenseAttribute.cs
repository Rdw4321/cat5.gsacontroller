﻿using ArmadilloCsharp;
using Cauldron.Interception;
using Grasshopper.Kernel;
using System;
using System.Reflection;

namespace GHComponent_AutoPile
{
    /// <summary>
    /// Attribute for checking license before running component
    /// </summary>
    public class CheckLicenseAttribute : Attribute, IMethodInterceptor
    {
        /// <summary>
        /// Check license before running component
        /// </summary>
        /// <param name="declaringType"></param>
        /// <param name="instance"></param>
        /// <param name="methodbase"></param>
        /// <param name="values"></param>
        public void OnEnter(Type declaringType, object instance, MethodBase methodbase, object[] values)
        {
            if (Cats5GrasshopperStartTracking.CanRun)
            {
                string appName = Cats5GrasshopperStartTracking.AppName;
                string featureName = instance.GetType().Name;
                TrackingInfo trackInfo = new TrackingInfo(appName, featureName);
                bool checkResult = trackInfo.LogFeature();
            }
            else
            {
                GH_Component gH_Component = (GH_Component)instance;
                gH_Component.ClearRuntimeMessages();
                throw new Exception("Can't find the AutoPile license!");
            }
        }

        /// <summary>
        /// throw exceptions
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool OnException(Exception e)
        {
            return true;
        }

        /// <summary>
        /// do nothing after logging
        /// </summary>
        public void OnExit()
        {
        }
    }
}
