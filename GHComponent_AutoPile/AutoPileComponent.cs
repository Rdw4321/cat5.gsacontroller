﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Grasshopper.Kernel;
using PileDesigndll;
using Rhino.Geometry;
using System.Windows.Forms;

namespace GHComponent_AutoPile
{
    public class AutoPile_Tower_FreeStyle : GH_Component
    {
        public AutoPile_Tower_FreeStyle() : base("AutoPile__Tower_FreeStyle", "GetNode", "Set pile with free style algorithm.", "AutoPile", "Algorithm")
        {
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.tower_freeStyle;
            }
        }

        public override Guid ComponentGuid => new Guid("d4a847e7-0b5b-43f6-b9c1-fc6034fc2efa");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("isRun", "R", "Switch.", GH_ParamAccess.item);
            pManager.AddTextParameter("csvPath", "csv", "csv file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("GSAPath", "gsa", "gsa file path.", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
        }
        [CheckLicense]
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare a variable for the input String
            string isRun = null;
            string csvPath = null;
            string GSAPath = null;


            string outMessage = null;
            // Use the DA object to retrieve the data inside the first input parameter.
            // If the retieval fails (for example if there is no data) we need to abort.
            if (!DA.GetData(0, ref isRun)
                || !DA.GetData(1, ref csvPath)
                || !DA.GetData(2, ref GSAPath)
                                            )
            { return; }

            // If the retrieved data is Nothing, we need to abort.
            // We're also going to abort on a zero-length String.
            if (csvPath == null
                || GSAPath == null
                || isRun == null
                ) { return; }

            if (csvPath.Length == 0
                || GSAPath.Length == 0) { return; }

            if (isRun == "True")
            {
                MyInterface.Linker A = new MyInterface.Linker(csvPath, GSAPath, MyInterface.Linker.FunType.TowerPileDesign_FreeStyle);
            }
            // Use the DA object to assign a new String to the first output parameter.
        }
    }
    public class AutoPile_PodiumPileDesign_TriangleGrid : GH_Component
    {
        public AutoPile_PodiumPileDesign_TriangleGrid() : base("AutoPile_PodiumPileDesign", "PA_1", "Arrange piles algorithm 1.", "AutoPile", "Algorithm")
        {
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.Podium_triangleGrid;
            }
        }

        public override Guid ComponentGuid => new Guid("d8f05e92-2161-4bc1-825d-20634e92cc50");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("isRun", "R", "Switch.", GH_ParamAccess.item);
            pManager.AddTextParameter("csvPath", "csv", "csv file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("GSAPath", "gsa", "gsa file path.", GH_ParamAccess.item);

        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
        }
        [CheckLicense]
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare a variable for the input String
            string isRun = null;
            string csvPath = null;
            string GSAPath = null;

            string outMessage = null;
            // Use the DA object to retrieve the data inside the first input parameter.
            // If the retieval fails (for example if there is no data) we need to abort.
            if (!DA.GetData(0, ref isRun)
                || !DA.GetData(1, ref csvPath)
                || !DA.GetData(2, ref GSAPath))
            { return; }

            // If the retrieved data is Nothing, we need to abort.
            // We're also going to abort on a zero-length String.
            if (csvPath == null
                || GSAPath == null
                || isRun == null
                ) { return; }

            if (csvPath.Length == 0
                || GSAPath.Length == 0) { return; }

            if (isRun == "True")
            {
                MyInterface.Linker A = new MyInterface.Linker(csvPath, GSAPath, MyInterface.Linker.FunType.PodiumPileDesign_TriangleGrid);
            }
            // Use the DA object to assign a new String to the first output parameter.

        }
    }

    public class AutoPile_TowerPileDesign_TriangleGrid : GH_Component
    {
        public AutoPile_TowerPileDesign_TriangleGrid() : base("AutoPile_Component4Tower", "PileForTower", "Arrange piles algorithm tower.", "AutoPile", "Algorithm")
        {
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.tower_triangleGrid;
            }
        }

        public override Guid ComponentGuid => new Guid("e23b4019-e69c-4d2e-9290-c53ee2713a2a");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("isRun", "R", "Switch.", GH_ParamAccess.item);
            pManager.AddTextParameter("csvPath", "csv", "csv file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("GSAPath", "gsa", "gsa file path.", GH_ParamAccess.item);

        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {

        }

        [CheckLicense]
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare a variable for the input String
            string isRun = null;
            string csvPath = null;
            string GSAPath = null;

            string outMessage = null;
            // Use the DA object to retrieve the data inside the first input parameter.
            // If the retieval fails (for example if there is no data) we need to abort.
            if (!DA.GetData(0, ref isRun)
                || !DA.GetData(1, ref csvPath)
                || !DA.GetData(2, ref GSAPath))
            { return; }

            // If the retrieved data is Nothing, we need to abort.
            // We're also going to abort on a zero-length String.
            if (csvPath == null
                || GSAPath == null
                || isRun == null
                ) { return; }

            if (csvPath.Length == 0
                || GSAPath.Length == 0) { return; }

            if (isRun == "True")
            {
                MyInterface.Linker A = new MyInterface.Linker(csvPath, GSAPath, MyInterface.Linker.FunType.TowerPileDesign_TriangleGrid);
            }
            // Use the DA object to assign a new String to the first output parameter.
        }
    }

    public class AutoPile_TowerPileDesign_Priority : GH_Component
    {
        public AutoPile_TowerPileDesign_Priority() : base("AutoPile_Tower Pile Design with priority method", "Update", "Design piles under a tower with DW method, " +
            "about the detial of the algorithm, please read the introduce of the algorithm in user guide.",
            "AutoPile", "Algorithm")
        {
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.tower_Dmethod;
            }
        }

        public override Guid ComponentGuid => new Guid("3466a70e-fff1-454b-aace-7b97ccb7d147");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("isRun", "R", "Start the analysis in the component.", GH_ParamAccess.item);
            pManager.AddTextParameter("csvPath", "csv", "csv data file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("GSAPath", "gsa", "gsa model file path.", GH_ParamAccess.item);

        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {

        }

        [CheckLicense]
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare a variable for the input String
            string isRun = null;
            string csvPath = null;
            string GSAPath = null;

            string outMessage = null;
            // Use the DA object to retrieve the data inside the first input parameter.
            // If the retieval fails (for example if there is no data) we need to abort.
            if (!DA.GetData(0, ref isRun)
                || !DA.GetData(1, ref csvPath)
                || !DA.GetData(2, ref GSAPath))
            { return; }

            // If the retrieved data is Nothing, we need to abort.
            // We're also going to abort on a zero-length String.
            if (csvPath == null
                || GSAPath == null
                || isRun == null
                ) { return; }

            if (csvPath.Length == 0
                || GSAPath.Length == 0) { return; }

            if (isRun == "True")
            {
                MyInterface.Linker A = new MyInterface.Linker(csvPath, GSAPath, MyInterface.Linker.FunType.TowerPileDesign_Priority);
            }
            // Use the DA object to assign a new String to the first output parameter.
        }
    }

    public class AutoPile_ComponentGetGSARaftNode : GH_Component
    {
        public AutoPile_ComponentGetGSARaftNode() : base("AutoPile_ComponentGetGSARaftNode", "GetNode", "Output gsa node to csv file.", "AutoPile", "Preparation")
        {
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.GetPoints;
            }
        }

        public override Guid ComponentGuid => new Guid("3c5505bb-7022-4844-869f-a384fd287301");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("isRun", "R", "Switch.", GH_ParamAccess.item);
            pManager.AddTextParameter("csvPath", "csv", "csv file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("GSAPath", "gsa", "gsa file path.", GH_ParamAccess.item);
            pManager.AddTextParameter("NodeList.", "NList", "gsa node list.", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
        }
        [CheckLicense]
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare a variable for the input String
            string isRun = null;
            string csvPath = null;
            string GSAPath = null;
            string nodeList = null;

            string outMessage = null;
            // Use the DA object to retrieve the data inside the first input parameter.
            // If the retieval fails (for example if there is no data) we need to abort.
            if (!DA.GetData(0, ref isRun)
                || !DA.GetData(1, ref csvPath)
                || !DA.GetData(2, ref GSAPath)
                || !DA.GetData(3, ref nodeList))
            { return; }

            // If the retrieved data is Nothing, we need to abort.
            // We're also going to abort on a zero-length String.
            if (csvPath == null
                || GSAPath == null
                || isRun == null
                ) { return; }

            if (csvPath.Length == 0
                || GSAPath.Length == 0) { return; }


            if (isRun == "True")
            {
                MyInterface.Linker A = new MyInterface.Linker(csvPath, GSAPath, MyInterface.Linker.FunType.getGSANode, nodeList);
            }

            // Use the DA object to assign a new String to the first output parameter.

        }
    }

}