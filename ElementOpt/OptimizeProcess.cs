﻿using System;
using System.Collections.Generic;
using System.Text;
using GSAdll;
using MyInterface;
using System.Windows.Forms;
using Interop.Gsa_10_0;

namespace ElementOpt
{
    public class OptimizeProcess
    {
        Dictionary<string, GSA_Element> DicElements = new Dictionary<string, GSA_Element>();
        public OptimizeProcess()
        {
            GetEleAxialFroceAndUpdateSec();
        }

        public void GetEleAxialFroceAndUpdateSec()
        {
            GSA_TotalModel.GetAllSections();
            foreach (GSA_Element  EL in GSA_TotalModel.AllEls )
            {
                EL.updateSeckByFx("C1");
            }

            GSA_TotalModel.updateElements();

        }


    }
}
