﻿Public Class GSA_Element
    Public NodeList As New List(Of Long)
    Public ID As Integer
    Public strELType As String
    Public EL As New GsaElement
    Public Sec As GSA_Section_1D
    Public Result As GsaResults

    Sub New(fields As String())
        Me.ID = fields(0)
        EL.Property = fields(1)
        NodeList.Add(fields(2))
        NodeList.Add(fields(3))
    End Sub
    Sub New(EL As GsaElement)
        Me.EL = EL
        Me.ID = EL.Ref
        type2Str(EL.eType)
    End Sub

    Sub add2GSA()
        Dim comList As New GSAComList
        comList.AddArray({"SET, EL.2 ", EL.Ref, "", "NO_RGB", strELType, EL.Property, EL.Group, EL.Topo})
        comList.RunCommand()
    End Sub

    Sub updateSeckByFx(LoadCase As String)
        Result = GetForce(LoadCase)
        For Each aSec In GSA_TotalModel.AllSectionDicbyNo.Values
            If aSec.Capacity <> -1 AndAlso Math.Abs(Result.dynaResults(0)) < aSec.Capacity Then
                EL.Property = aSec.CurSec.Ref
                Exit For
            End If
        Next

    End Sub
    Function GetForce(LoadCase As String) As GsaResults
        Dim comList As New GSAComList
        Dim Ret As GsaResults() = comList.Get1DRet(ResHeader.REF_FORCE_EL1D, LoadCase, ID)
        For Each GR In Ret
            For i = 0 To UBound(Ret(0).dynaResults)
                If Ret(0).dynaResults(i) < GR.dynaResults(i) Then
                    Ret(0).dynaResults(i) = GR.dynaResults(i)
                End If
            Next
        Next
        Return Ret(0)
    End Function

    Sub type2Str(etype As Int16)
        Select Case etype
            Case 2
                strELType = "BEAM"

        End Select

    End Sub

End Class
