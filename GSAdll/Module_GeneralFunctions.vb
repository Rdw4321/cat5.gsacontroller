﻿Imports System.IO

Public Module Module_GeneralFunctions
    Public MyBook As Workbook, MySheet As Worksheet
    Public InputDicFromExcel As Dictionary(Of String, List(Of String))
    Public EF As New CommonFunctions.ExcelFunctions
    Public TF As New CommonFunctions.ClassMyTextFunction
    Public InputDicFromdwIO As Dictionary(Of String, String)
    Function Delete_to(original_text As String) As Array
        Dim Arr()
        Dim i As Long, j As Long, k As Long
        Dim ret_arrList As New List(Of String)
        original_text = Replace(original_text, " to ", "to")

        Arr = MF.CutStr(original_text, " ")

        i = 1
        j = 1
        For i = 1 To UBound(Arr)

            If InStr(Arr(i), "to") <> 0 Then

                Dim St, En As Long
                St = MF.CutStr(Arr(i), "to")(1)
                En = MF.CutStr(Arr(i), "to")(2)

                For k = St To En
                    ret_arrList.Add(CStr(k))
                    j = j + 1
                Next k

            Else
                ret_arrList.Add(Arr(i))
                j = j + 1
            End If

        Next
        Return ret_arrList.ToArray
    End Function

    Sub GetNodeLocations(nodeString As String, csvFilePath As String, gsaFilePath As String)
        Dim NodeListArray As Array = Delete_to(nodeString)
        Dim retList As New List(Of String)

        For Each nodeNum As Long In NodeListArray
            Dim aN As New GSA_Node(nodeNum)
            retList.Add(String.Join(",", {aN.ID, String.Join(",", aN.Coordination)}))
        Next

        If System.IO.File.Exists(csvFilePath) = False Then
            Dim aFN = File.Create(csvFilePath)
            aFN.Close()
        End If
        System.IO.File.WriteAllLines(csvFilePath, retList.ToArray)
        'MsgBox("Give Node Coordination Done.")
    End Sub

    Sub GetInputFromExcel()
        Dim MyXL As Microsoft.Office.Interop.Excel.Application
        MyXL = GetObject(, "Excel.Application")
        MyBook = MyXL.Workbooks("GSA Optimization IO.xlsm")
        MySheet = MyBook.Worksheets("Input")
        InputDicFromExcel = New Dictionary(Of String, List(Of String))

        Dim i = 3
        Do While MySheet.Cells(i, 2).text <> ""
            Dim j = 2
            Dim ValueList As New List(Of String)
            Do While MySheet.Cells(i, j).text <> ""
                ValueList.Add(MySheet.Cells(i, j).text)
                j += 1
            Loop
            InputDicFromExcel.Add(MySheet.Cells(i, 1).text, ValueList)
            i += 1
        Loop

    End Sub
    Sub OutputRet()
        TF.WriteDWout(InputDicFromdwIO, "dwIO.txt")
    End Sub

    Function ConcretFc(degree As String)
        Dim fc

        Return fc
    End Function

    'Public Class textLink
    '    Public fileReader As System.IO.StreamReader
    '    Sub New(Fn As String)
    '        fileReader = My.Computer.FileSystem.OpenTextFileReader(Fn)
    '    End Sub
    '    Public Sub release()
    '        fileReader.Close()
    '        fileReader = Nothing
    '    End Sub
    'End Class


End Module
