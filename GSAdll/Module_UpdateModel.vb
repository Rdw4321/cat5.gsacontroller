﻿Public Module Module_UpdateModel
    Public OptParaList As List(Of OptPara)


    Sub UpdateModelbyOptPara()

        OptParaList = New List(Of OptPara)
        GSA_TotalModel.GetAllSections()
        GSA_TotalModel.Get2DProperties()

        For Each item In InputDicFromdwIO
            If isOptParaIn(item.Key) Then
                OptParaList.Add(New OptPara(item.Key, item.Value))
            End If
        Next

        GSA_TotalModel.UpdateSections()
    End Sub

    Public Class OptPara
        Public Name As String
        Public Scale As Single, SteelRatio As Single
        Public Section_ID As Integer
        Public SectionType As SecType
        Public ParaType As OptType
        Sub New(ref As Integer, sectiontype As SecType)
            If sectiontype = SecType.EL_1D Then
                Name = String.Format("C_{0}", {ref})
            ElseIf sectiontype = SecType.EL_2D Then
                Name = String.Format("W_{0}", {ref})
            End If
        End Sub

        Sub New(Name As String, Value As String)
            Me.Name = Name
            If Name.Contains("W") Then
                SectionType = SecType.EL_2D
            Else
                SectionType = SecType.EL_1D
            End If

            Section_ID = MF.CutStr(Name, "_")(2)

            If Value.Contains("%") Then
                SteelRatio = CDec(Value)
                ParaType = OptType.Opt_SteelRatio
            Else
                Scale = CDec(Value)
                ParaType = OptType.Opt_Scale
            End If
            MF.WatchPro(GSA_TotalModel.ShowBox, String.Format("Get Optimization Parameter {0}:{1}", {Name, Value}))

            UpdateSectoin()
        End Sub

        Enum SecType
            EL_1D
            EL_2D
        End Enum
        Enum OptType
            Opt_Scale
            Opt_SteelRatio
        End Enum
        Public Sub UpdateSectoin()
            If SectionType = SecType.EL_1D Then
                GSA_TotalModel.AllSectionDicbyNo.Item(CStr(Section_ID)).UpdatebyOptPara(Me)
            ElseIf SectionType = SecType.EL_2D Then
                GSA_TotalModel.All2DPropertyDicbyNo.Item(CStr(Section_ID)).UpdatebyOptPara(Me)
            End If
        End Sub
    End Class
End Module

