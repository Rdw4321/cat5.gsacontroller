﻿Imports System.Math
Public Module Module_ModelResultSort
    Public TotalForce_DL, TotalForce_LL As Single, Weight As String
    Public StoreyDrift_x = 99999, StoreyDrift_y = 99999


    Sub GetModelResult()

        GSA_TotalModel.getFloos()
        GetDLAndLL()
        UpdateDWIO("DL", TotalForce_DL)
        UpdateDWIO("LL", TotalForce_LL)
        UpdateDWIO("Weigth", Weight)

        UpdateDWIO("SvsG_x", GetSGRatio(0))
        UpdateDWIO("SvsG_y", GetSGRatio(1))
        GetStoryDrift()
        UpdateDWIO("StoreyDrift_x", StoreyDrift_x)
        UpdateDWIO("StoreyDrift_y", StoreyDrift_y)
        GetAllAcr()
        UpdateDWIO("CalTime", DateAndTime.Now.ToString("yyyy/MM/dd hh:mm:ss"))
    End Sub

    Function GetTotalRection(LoadCase As String, Optional dir As String = "z")
        Dim retNum As Integer
        If dir.ToLower = "z" Then
            retNum = 6
        ElseIf dir.ToLower = "x" Then
            retNum = 4
        ElseIf dir.ToLower = "y" Then
            retNum = 5
        End If
        Dim comList As New GSAComList
        comList.AddArray({"GET,TOTAL_FORCE", LoadCase, "REACT"})
        Return comList.RunCommand(retNum)
    End Function
    ''' <summary>
    ''' 计算刚重比
    ''' </summary>
    Function GetSGRatio()
        Dim Node As New GSA_Node(InputDicFromExcel.Item("刚重比计算用节点号")(0))
        Dim dis_x As GsaResults = Node.GetDisp(InputDicFromExcel.Item("刚重计算用工况[x y]")(0))
        Dim dis_y As GsaResults = Node.GetDisp(InputDicFromExcel.Item("刚重计算用工况[x y]")(1))

        Dim H = InputDicFromExcel.Item("塔楼高度")(0)

        Dim qx = Abs(2 * GetTotalRection(Right(InputDicFromExcel.Item("刚重计算用工况[x y]")(0), 1), "x") / H)
        Dim qy = Abs(2 * GetTotalRection(Right(InputDicFromExcel.Item("刚重计算用工况[x y]")(1), 1), "y") / H)

        Dim EJd_x = 11 * H ^ 4 * qx / 120 / (dis_x.dynaResults(0) / 1000)
        Dim EJd_y = 11 * H ^ 4 * qy / 120 / (dis_y.dynaResults(1) / 1000)

        Dim SvsG_x = EJd_x / H ^ 2 / （1.2 * TotalForce_DL + 1.4 * TotalForce_LL）
        Dim SvsG_y = EJd_y / H ^ 2 / （1.2 * TotalForce_DL + 1.4 * TotalForce_LL）

        Return {SvsG_x, SvsG_y}

    End Function
    Sub GetDLAndLL()
        TotalForce_DL = GetTotalRection(Right(InputDicFromExcel.Item("恒载工况[纯数字格式]")(0), 1))
        TotalForce_LL = GetTotalRection(Right(InputDicFromExcel.Item("活载工况[纯数字格式]")(0), 1))

        If InputDicFromExcel.Item("材料统计工况[纯数字格式]").Count > 0 Then
            Weight = GetTotalRection(Right(InputDicFromExcel.Item("材料统计工况[纯数字格式]")(0), 1))
        End If
    End Sub


    Sub UpdateDWIO(Key As String, Value As String)
        If Value <> "" Then
            If InputDicFromdwIO.ContainsKey(Key) Then
                InputDicFromdwIO.Item(Key) = Value
            Else
                InputDicFromdwIO.Add(Key, Value)
            End If
        End If
    End Sub

    Sub GetStoryDrift()
        For Each ptListstr In InputDicFromExcel.Item("位移角点集")
            Dim DriftPtList As New List(Of GSA_Node)
            For Each pt As String In Delete_to(ptListstr)
                Dim aPt As New GSA_Node(pt)
                aPt.getFloor()
                DriftPtList.Add(aPt)
            Next
            DriftPtList = DriftPtList.OrderBy(Function(x) x.Floor.Ref).Reverse.ToList()

            For Each pt In DriftPtList
                If pt.Floor.Ref <> 1 Then
                    pt.Floor.GetDrift(DriftPtList(DriftPtList.IndexOf(pt) + 1), pt, InputDicFromExcel.Item("位移角计算用工况[x y]")(0), InputDicFromExcel.Item("位移角计算用工况[x y]")(1))
                End If

            Next
        Next

        For Each fl In GSA_TotalModel.FloorList
            StoreyDrift_x = Min(fl.Drift_x, StoreyDrift_x)
            StoreyDrift_y = Min(fl.Drift_y, StoreyDrift_x)
        Next
    End Sub

    Sub GetAllAcr()
        GSA_TotalModel.GetAllEls()
        For Each EL In GSA_TotalModel.GSAAllEls

            If EL.eType = 2 AndAlso InputDicFromdwIO.ContainsKey("C_" & EL.Property) Then
                GSA_TotalModel.AllSectionDicbyNo.Item(EL.Property).GetACR(InputDicFromExcel.Item("轴压比工况[墙_柱]")(1) & "min", EL.Ref)
            ElseIf EL.eType = 5 AndAlso InputDicFromdwIO.ContainsKey("W_" & EL.Property) Then
                GSA_TotalModel.All2DPropertyDicbyNo.Item(EL.Property).GetACR(InputDicFromExcel.Item("轴压比工况[墙_柱]")(0) & "min", EL.Ref)
            End If

        Next
        Dim ArcList As New List(Of String)
        For Each optPara In InputDicFromdwIO
            If isOptParaIn(optPara.Key) Then
                ArcList.Add(optPara.Key)
            End If
        Next

        For Each Arc In ArcList
            If Arc.Contains("C_") Then
                UpdateDWIO(Arc & "_Arc", GSA_TotalModel.AllSectionDicbyNo.Item(MF.CutStr(Arc, "_")(2)).Acr)
            ElseIf Arc.Contains("W_") Then
                UpdateDWIO(Arc & "_Arc", GSA_TotalModel.All2DPropertyDicbyNo.Item(MF.CutStr(Arc, "_")(2)).Acr)
            End If
        Next

    End Sub
    Function isOptParaIn(name As String)
        If (name.Contains("C_") OrElse name.Contains("W_")) AndAlso Not name.Contains("Arc") Then
            Return True
        End If
        Return False
    End Function

End Module
