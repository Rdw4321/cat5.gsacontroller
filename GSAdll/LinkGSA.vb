﻿Imports Interop.gsa_8_7
Public Module GSAControl
    Public GSAobj As Interop.gsa_8_7.ComAuto
    Public MF As New CommonFunctions.Myfunction
    Enum GSA_version
        v8_7
        v9_0
        v10_0
    End Enum
    Public Class LinkGSA
        Public isGoOn As Boolean
        Public Sub New(gwbFileName As String, Optional GSAversion As GSA_version = GSA_version.v8_7, Optional UnitType As Boolean = False)
            'On Error GoTo errHandle
            Select Case GSAversion
                Case GSA_version.v9_0
                    'GSAobj = New Gsa_9_0.ComAuto
                    GSAobj = CreateObject("Gsa_9_0.ComAuto")
                Case GSA_version.v8_7
                    'GSAobj = New Gsa_8_7.ComAuto
                    GSAobj = CreateObject("Gsa.ComAuto")
                Case GSA_version.v10_0
                    GSAobj = CreateObject("Gsa_10_0.ComAuto")
            End Select

            If GSAobj.Open(gwbFileName) = 1 Then
                GoTo errHandle
            End If

            isGoOn = True
            Dim comList As New GSAComList
            comList.SetGSAUnit(UnitType)
            Exit Sub
errHandle:
            MsgBox("Can not link to model " & gwbFileName)
            isGoOn = False
        End Sub
        '##################################################################
        'code for test 
        'Public Sub AddANode()
        '    Dim aNode As New GSA_Node(1, {1.0#, 1.0#, 1.0#},, {1.0#, 1.0#, 1.0#, 0#, 0#, 0#})
        '    aNode.add2GSA()
        'End Sub

        Public Sub updateSupport(num As Long)
            Dim aNode As New GSA_Node(1)
            aNode.updateNode(, {12.0#, 1.0#, 12.0#, 1.0#, 12.0#, 1.0#})
        End Sub

        Public Sub ReleasGSA(ifSave As Boolean)
            If ifSave Then GSAobj.Save()
            GSAobj.Close()
        End Sub
    End Class

    Public Class GSAComList
        Inherits List(Of String)
        ''' <summary>
        ''' Link all the strings and arrays in the list.
        ''' <para>Arr:array combined by string and array. </para>
        ''' </summary>
        ''' <param name="Arr" > </param>
        Public Sub AddArray(Arr As Array)
            For i = LBound(Arr) To UBound(Arr)
                If TypeOf (Arr(i)) Is Array Then
                    For Each En1 In (Arr(i))
                        Me.Add(String.Join(",", En1))
                    Next
                Else
                    Me.Add(CStr((Arr(i))))
                End If
            Next
        End Sub
        ''' <summary>
        ''' i is the str number from left, start from 1.
        ''' </summary>
        Public Function RunCommand(Optional i As Int16 = 0) As String
            Dim ret As String
            On Error GoTo errHandle
            Dim comStr As String = String.Join(",", Me)
            comStr = comStr.ToUpper
            ret = GSAobj.GwaCommand(comStr)
            If ret = "0" Then
                GoTo errHandle
            End If

            If i = 0 Then
                Return ret
            Else
                Return MF.CutStr(ret, ",")(i)
            End If
            Exit Function
errHandle:
            'MsgBox("Can not run command " & comStr)
        End Function

        Public Function Get1DRet(DataRef As ResHeader, LoadCase As String, ID As Int16) As GsaResults()
            GSAobj.Output_Init_Arr(&H20, "default", LoadCase, DataRef, 5)
            Dim arrRes As GsaResults(), numComp As Int16
            GSAobj.Output_Extract_Arr(ID, arrRes, numComp)

            Return arrRes
        End Function

        Public Function Get2DRet(DataRef As ResHeader, LoadCase As String, ID As Int16) As GsaResults()
            GSAobj.Output_Init_Arr(&H2, "default", LoadCase, DataRef, 5)
            Dim arrRes As GsaResults(), numComp As Int16
            GSAobj.Output_Extract_Arr(ID, arrRes, numComp)
            Return arrRes
        End Function

        Sub SetGSAUnit(isMMmmN As Boolean)
            If isMMmmN Then
                GSAobj.GwaCommand("SET,UNIT_DATA,   LENGTH,  mm ")
                GSAobj.GwaCommand("SET,UNIT_DATA,   Disp,  mm")
                GSAobj.GwaCommand("SET,UNIT_DATA,   SECTION,  mm")
                GSAobj.GwaCommand("SET,UNIT_DATA,   Force,  N")
            Else
                GSAobj.GwaCommand("SET,UNIT_DATA,   LENGTH,  mm")
                GSAobj.GwaCommand("SET,UNIT_DATA,   Disp,  mm")
                GSAobj.GwaCommand("SET,UNIT_DATA,   Section,  mm")
                GSAobj.GwaCommand("SET,UNIT_DATA,   Force,  kN")
            End If
        End Sub


    End Class
End Module
