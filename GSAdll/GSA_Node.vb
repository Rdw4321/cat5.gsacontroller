﻿Imports System.Math
Public Class GSA_Node
    Public Coordination(2) As Double
    Public K(5) As Double 'Support stiffness 
    Public ID As Long 'Node
    Public r(5) As Integer  '
    Public GSANode As Interop.gsa_8_7.GsaNode
    Public Floor As GSA_Floor

    Sub New(num As String, Optional Coordination1 As Array = Nothing, Optional r1 As Array = Nothing, Optional K1 As Array = Nothing)
        Me.ID = num
        GSANode.Ref = num
        If IsNothing(Coordination1) = False Then
            Me.Coordination = Coordination1
        Else
            getCoordinate(num)
        End If

        If IsNothing(r1) = False Then
            Me.r = r1
        End If
        If IsNothing(K1) = False Then
            Me.K = K1
        End If
    End Sub
    Sub New(fields() As String)
        Me.ID = fields(0)

        Dim CoordinationArr = MF.CutStr(fields(1), "{", "}", ",")
        Me.Coordination(0) = CoordinationArr(2)
        Me.Coordination(1) = CoordinationArr(3)
        Me.Coordination(2) = CoordinationArr(4)

        If fields（2） = "Pin" Then
            Me.r = {1, 1, 1, 0, 0, 0}
        End If

    End Sub


    Public Sub add2GSA()
        Dim comList As New GSAComList
        comList.AddArray({"SET, NODE.1", ID, Coordination, "", r, K})
        comList.RunCommand()
    End Sub

    Public Sub getCoordinate(num As Long)
        GSAobj.NodeCoor(num, Coordination(0), Coordination(1), Coordination(2))
        For i = 0 To 2
            Me.Coordination(i) = Coordination(i) * 1000
        Next
    End Sub


    ''' <summary>
    ''' if a node has exist, updated the support of the node
    ''' </summary>
    ''' <param name="K1"></param>
    Public Sub updateNode(Optional r1 As Array = Nothing, Optional K1 As Array = Nothing)
        getCoordinate(ID)
        If IsNothing(r1) = False Then
            Me.r = r1
        End If
        If IsNothing(K1) = False Then
            Me.K = K1
        End If
        add2GSA()
    End Sub

    Public Function getRactionZ(loadCase As String)
        Dim comList As New GSAComList
        comList.AddArray({"GET,REACT_FORCE", ID, loadCase})
        Return comList.RunCommand(6)
    End Function

    Public Function GetDisp(LoadCase As String)
        Dim comList As New GSAComList
        Dim Ret = comList.Get1DRet(ResHeader.REF_DISP, LoadCase, ID)
        For Each GR In Ret
            For i = 0 To UBound(Ret(0).dynaResults)
                If Ret(0).dynaResults(i) < GR.dynaResults(i) Then
                    Ret(0).dynaResults(i) = GR.dynaResults(i)
                End If
            Next
        Next
        Return Ret(0)
    End Function

    Sub getFloor()
        For Each Fl In GSA_TotalModel.FloorList
            If Abs(Coordination(2) - Fl.Level) < 10 Then
                Floor = Fl
            End If
        Next
    End Sub
End Class
