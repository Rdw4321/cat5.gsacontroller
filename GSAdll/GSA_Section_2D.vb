﻿Imports System.Math
Public Class GSA_Section_2D
    Public Name, Type, Mat As String
    Public Ref As Int16
    Public Thickness As Single, Thickness_steel As Single
    Public BendingModifier, In_PlaneModifier, MassWeight As Single
    Public Steel_Ratio As String
    Public Acr As Single


    Sub New(ref As Int16)
        Dim Com As New GSAComList
        Com.AddArray({"GET,PROP_2D.2", ref})
        Dim ret = Com.RunCommand()
        Dim tempArr = MF.CutStr(ret, ",")

        Me.Ref = ref
        Name = tempArr(3)
        Mat = tempArr(6)
        Type = tempArr(7)
        Thickness = tempArr(8)
        BendingModifier = CSng(Replace(tempArr(10), "%", ""))
        In_PlaneModifier = CSng(Replace(tempArr(11), "%", ""))
        MassWeight = CSng(Replace(tempArr(12), "%", ""))

        If Not BendingModifier = 100 Then
            'Thickness_steel = (((BendingModifier / 100) - 1) * Thickness ^ 3 * E_RC) / E_Steel
        End If

    End Sub

    Sub update2GSA()
        Dim Com As New GSAComList

        Com.AddArray({"SET,PROP_2D.2", Ref, Name, "NO_RGB", "GLOBAL", Mat, Type, Thickness, 0, BendingModifier & "%", In_PlaneModifier & "%", "100%"})
        Dim ret = Com.RunCommand()
    End Sub

    Sub UpdatebyOptPara(Para As OptPara)
        If Para.ParaType = OptPara.OptType.Opt_Scale Then
            Thickness *= Para.Scale
        End If

        update2GSA()
    End Sub

    Sub GetACR(LoadCase As String, EL_ref As Long)
        Dim aCom = New GSAComList

        Dim Ret As GsaResults()
        Ret = aCom.Get2DRet(ResHeader.REF_STRESS_EL2D_PRJ, LoadCase, EL_ref)
        Dim stress = Ret(4).dynaResults(1) / 1000000.0

        Dim f_c = 16.7, f_s = 250
        Dim length = 1

        Dim A_total = length * Thickness
        Dim A_RC = length * (Thickness - Thickness_steel)
        Dim A_S = length * Thickness_steel
        Dim n = -(stress * A_total) / (f_c * A_RC + f_s * A_S)
        Acr = Max(Acr, n)
    End Sub




End Class
