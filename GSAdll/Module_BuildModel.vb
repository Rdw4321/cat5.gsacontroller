﻿Imports Microsoft.VisualBasic.FileIO
Public Module Module_BuildModel
    Public Sub Build_Main(GSAfile As String, Csvfile As String, CsvfileEL1D As String)
        Dim GSALink_obj As New LinkGSA(GSAfile, UnitType:=True, GSAversion:=GSA_version.v8_7)
        ReadcsvfileAndGenNodes(Csvfile)
        ReadcsvfileAndGen1DEL(CsvfileEL1D)
        'GSALink_obj.ReleasGSA(True)
    End Sub

    Sub ReadcsvfileAndGenNodes(csvFn As String)
        Dim nodeList As New List(Of GSA_Node)
        Dim tfp As New TextFieldParser(csvFn) With
        {
        .Delimiters = New String() {","},
        .TextFieldType = FieldType.Delimited
        }
        Dim TitleList = tfp.ReadFields()

        Do Until tfp.EndOfData = True
            Dim fields = tfp.ReadFields()
            If Not IsNothing(fields) Then
                Dim aN As New GSA_Node(fields)
                nodeList.Add(aN)
            End If
        Loop
        tfp.Close()
        For Each GSAnode In nodeList
            GSAnode.add2GSA()
        Next
    End Sub

    Sub ReadcsvfileAndGen1DEL(csvFn As String)
        Dim EL1DList As New List(Of GSA_Element)
        Dim tfp As New TextFieldParser(csvFn) With
        {
        .Delimiters = New String() {","},
        .TextFieldType = FieldType.Delimited
        }
        Dim TitleList = tfp.ReadFields()

        Do Until tfp.EndOfData = True
            Dim fields = tfp.ReadFields()
            If Not IsNothing(fields) Then
                Dim aN As New GSA_Element(fields)
                EL1DList.Add(aN)
            End If
        Loop
        tfp.Close()

        For Each GSAEL In EL1DList
            GSAEL.add2GSA()
        Next
    End Sub

End Module



