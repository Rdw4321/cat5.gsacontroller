﻿Imports System.Math
Public Class GSA_Floor
    Public Name As String
    Public Level, Height_low As Single
    Public Ref As Int16
    Public Drift_x = 99999, Drift_y = 99999

    Sub New(GSALine As String)
        Dim temparr = MF.CutStr(GSALine, ",")
        Ref = temparr(2)
        Name = temparr(3)
        Level = temparr(6)
    End Sub

    Sub GetDrift(ptLow As GSA_Node, ptHigh As GSA_Node, x_LoadCase As String, y_LoadCase As String)
        Dim dis_x_low, dis_y_low
        If ptLow.Floor.Ref = 1 Then
            dis_x_low = 0
            dis_y_low = 0
        Else
            dis_x_low = ptLow.GetDisp(x_LoadCase).dynaResults(0)
            dis_y_low = ptLow.GetDisp(y_LoadCase).dynaResults(1)
        End If


        Dim dis_x_high = ptHigh.GetDisp(x_LoadCase).dynaResults(0)
        Dim dis_y_high = ptHigh.GetDisp(y_LoadCase).dynaResults(1)

        Drift_x = Min(Abs(CInt(Height_low / (dis_x_high - dis_x_low))), Drift_x)
        Drift_x = Min(Abs(CInt(Height_low / (dis_y_high - dis_y_low))), Drift_y)
    End Sub
End Class
