﻿Imports System.Windows
Public Class GSA_TotalModel
    Public Shared AllEls As List(Of GSA_Element)
    Public Shared GSAAllEls() As GsaElement
    Public Shared AllSectionDicbyNo As Dictionary(Of String, GSA_Section_1D)
    Public Shared All2DPropertyDicbyNo As Dictionary(Of String, GSA_Section_2D)
    Public Shared ShowBox As Forms.TextBox
    Public Shared FloorList As List(Of GSA_Floor)

    Shared Function getReaction(LC As String, Op As String)
        Dim comList As New GSAComList
        comList.AddArray({"GET,TOTAL_FORCE", LC, Op})
        Return comList.RunCommand(6)
    End Function
    Shared Sub getFloos()
        FloorList = New List(Of GSA_Floor)
        Dim floorNum As Int16
        Dim comList As New GSAComList
        comList.AddArray({"HIGHEST,GRID_PLANE"})
        floorNum = comList.RunCommand()

        For i = 1 To floorNum
            comList = New GSAComList
            comList.AddArray({"GET,GRID_PLANE", i})
            Dim ret As String
            ret = comList.RunCommand()
            Dim aFL As New GSA_Floor(ret)
            FloorList.Add(aFL)
        Next

        FloorList = FloorList.OrderBy(Function(x) x.Level).ToList()

        For i = 1 To FloorList.Count - 1
            FloorList(i).Height_low = FloorList(i).Level - FloorList(i - 1).Level
        Next
        FloorList.First.Height_low = 0
    End Sub


    Shared Sub GetAllSections()
        Dim Com As New GSAComList
        AllSectionDicbyNo = New Dictionary(Of String, GSA_Section_1D)
        Dim SecIDs As New List(Of Integer)

        Com.Add("HIGHEST,PROP_SEC")
        Dim ret = Com.RunCommand()
        For i = 1 To CInt(ret)
            SecIDs.Add(i)
        Next
        Dim SecInGSA() As GsaSection
        GSAobj.Sections(SecIDs.ToArray, SecInGSA)


        For Each OSec In SecInGSA
            Dim aSec As New GSA_Section_1D(OSec)
            AllSectionDicbyNo.Add(aSec.CurSec.Ref, aSec)
        Next
    End Sub

    Shared Sub Get2DProperties()
        Dim Com As New GSAComList
        All2DPropertyDicbyNo = New Dictionary(Of String, GSA_Section_2D)
        Dim SecIDs As New List(Of Integer)

        Com.Add("HIGHEST,PROP_2D")
        Dim ret = Com.RunCommand()
        For i = 1 To CInt(ret)
            Dim Property2D As New GSA_Section_2D(i)
            All2DPropertyDicbyNo.Add(i, Property2D)
        Next
    End Sub

    Shared Sub GetAllEls()
        AllEls = New List(Of GSA_Element)
        Dim Com As New GSAComList
        Com.Add("HIGHEST,EL.1")
        Dim ret = Com.RunCommand()
        Dim ELIDs As New List(Of Integer)

        For i = 1 To CInt(ret)
            ELIDs.Add(i)
        Next

        GSAobj.Elements(ELIDs.ToArray, GSAAllEls)

        For Each OEL In GSAAllEls
            Dim aSec As New GSA_Element(OEL)
            AllEls.Add(aSec)
        Next
    End Sub

    Shared Sub updateElements()
        GSAobj.Delete("RESULTS")
        For Each EL In AllEls
            EL.add2GSA()
        Next
    End Sub

    Shared Sub UpdateSections()
        GSAobj.Delete("RESULTS")
        For Each item In AllSectionDicbyNo.Values
            item.Set2GSA()
        Next
    End Sub

    Shared Sub ModelRunMain(FileName As String)
        MF.WatchPro(ShowBox, "Start text.")
        Dim GSAApp = New GSAdll.GSAControl.LinkGSA(FileName)
        If GSAApp.isGoOn Then
            GetInputFromExcel()
            InputDicFromdwIO = TF.GetDWIn("dwIO.txt")
            GSAobj.Delete("RESULT")
            UpdateModelbyOptPara()
            MF.WatchPro(ShowBox, "Start Analyse.")
            GSAobj.Analyse()
            GetModelResult()
            OutputRet()
        End If

        GSAApp.ReleasGSA(False)
        'MsgBox("Test Done.")
        MF.WatchPro(ShowBox, "Test Done.")
    End Sub

    Shared Sub genOptSectionList(FileName As String)


        Dim GSAApp = New GSAdll.GSAControl.LinkGSA(FileName)

        GSA_TotalModel.GetAllSections()
        GSA_TotalModel.Get2DProperties()
        InputDicFromdwIO = New Dictionary(Of String, String)

        For Each Sec_1d In GSA_TotalModel.AllSectionDicbyNo.Values
            If Sec_1d.CurSec.SectDesc.Contains("01") OrElse Sec_1d.CurSec.SectDesc.Contains("51") Then
                Dim aIO As New OptPara(Sec_1d.CurSec.Ref, OptPara.SecType.EL_1D)
                InputDicFromdwIO.Add(aIO.Name, "1")
            End If
        Next

        For Each Sec_2d In GSA_TotalModel.All2DPropertyDicbyNo.Values
            If CStr(Sec_2d.Thickness).Contains("01") OrElse CStr(Sec_2d.Thickness).Contains("51") Then
                Dim aIO As New OptPara(Sec_2d.Ref, OptPara.SecType.EL_2D)
                InputDicFromdwIO.Add(aIO.Name, "1")
            End If
        Next
        GenWeigthCalLoadCaseAndCombination()
        OutputRet()
        GSAApp.ReleasGSA(True)
        MsgBox("生成IO文件完成")
    End Sub

    Shared Sub GenWeigthCalLoadCaseAndCombination()
        GetInputFromExcel()
        Dim PropertyList As New List(Of String)

        For Each item In InputDicFromdwIO
            If isOptParaIn(item.Key) Then
                If item.Key.Contains("C") Then
                    PropertyList.Add("PB" & MF.CutStr(item.Key, "_")(2))
                ElseIf item.Key.Contains("W") Then
                    PropertyList.Add("PA" & MF.CutStr(item.Key, "_")(2))
                End If
            End If
        Next

        Dim PropertyListString As String = String.Join(" ", PropertyList)
        Dim comList As New GSAComList
        If InputDicFromExcel.Item("材料统计工况[纯数字格式]").Count > 0 Then

            comList.AddArray({"SET,LOAD_TITLE.1 ", InputDicFromExcel.Item("材料统计工况[纯数字格式]")(0), "Weight", "DEAD "})
            comList.RunCommand()

            comList = New GSAComList
            comList.AddArray({"SET,LOAD_GRAVITY.1", PropertyListString, InputDicFromExcel.Item("材料统计工况[纯数字格式]")(0), "", "", "-1"})
            comList.RunCommand()
        End If

        comList = New GSAComList
        comList.AddArray({"SET,COMBINATION", 1, "墙轴压比组合"， "1.2A1 + 0.6A2"})
        comList.RunCommand()

        comList = New GSAComList
        comList.AddArray({"SET,COMBINATION", 2, "柱轴压比组合"， "1.2A1 + 0.6A2 + (A3 or A4 or A5 or A6)"})
        comList.RunCommand()
    End Sub

End Class






