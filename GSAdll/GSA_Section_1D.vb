﻿Imports System.Math
Imports Interop.gsa_8_7
Public Class GSA_Section_1D
    Public CurSec As GsaSection
    Public DicOfOptionalGsaSec As New Dictionary(Of Single, GsaSection) 'must sort increasely
    Public Capacity As Single = -1
    Public Optimise_Factor As Single = 1.0
    Public Aper = 1.3, Iyyper = 1, Izzper = 1
    Public Area As Single
    Public Acr As Single

    Dim Hs, Bs, tw, tf, Ds, t, tf90, tw90 As Single
    Dim GSAName As String
    Dim SRatio As Double
    Dim RCMat As String
    Dim Ag As String
    Dim RCDim As String
    Dim SteelMat As String
    Dim SteelShape As String
    Dim Ast As String
    Dim SteelDim As String


    Public Sub New(Optional CurSec As GsaSection = Nothing)
        Me.CurSec = CurSec
        GetCapcity()
    End Sub

    Public Sub GetCapcity()
        If CurSec.Name <> "" Then
            Dim tempStrArr() As String = MF.CutStr(CurSec.Name, "_")
            If tempStrArr.Length = 3 Then
                Capacity = tempStrArr(2)
            End If
        End If
    End Sub
    Sub UpdateSecbyForce(Force As Single)
        For Each OptSec In DicOfOptionalGsaSec
            If Abs(OptSec.Key) > Abs(Force) Then
                CurSec = OptSec.Value
            End If
        Next
    End Sub

    Sub UpdatebyOptPara(OptPara As OptPara)
        Dim SecScale = OptPara.Scale
        Dim SecStr = CurSec.SectDesc
        Dim UnitModifier As Int16

        If MF.CutStr(SecStr, "%")(3) > 100 Then UnitModifier = 1 Else UnitModifier = 1000

        If InStr(SecStr, "%C%") <> 0 Then
            SecStr = "STD%C%" & Int(MF.CutStr(SecStr, "%")(3) * SecScale * UnitModifier)
        ElseIf InStr(SecStr, "%R%") <> 0 Then
            SecStr = "STD%R%" & Int(MF.CutStr(SecStr, "%")(3) * SecScale * UnitModifier) & "%" & Int(MF.CutStr(SecStr, "%")(4) * SecScale * UnitModifier)
        ElseIf InStr(SecStr, "%RHS%") <> 0 Then
            SecStr = "STD%RHS%" & Int(MF.CutStr(SecStr, "%")(3) * SecScale * UnitModifier) & "%" & Int(MF.CutStr(SecStr, "%")(4) * SecScale * UnitModifier) & "%" & Int(MF.CutStr(SecStr, "%")(5)) & "%" & Int(MF.CutStr(SecStr, "%")(6))
        ElseIf InStr(SecStr, "%I%") <> 0 Then
            SecStr = "STD%I%" & Int(MF.CutStr(SecStr, "%")(3) * SecScale * UnitModifier) & "%" & Int(MF.CutStr(SecStr, "%")(4) * SecScale * UnitModifier) & "%" & Int(MF.CutStr(SecStr, "%")(5)) & "%" & Int(MF.CutStr(SecStr, "%")(6))
        End If

        CurSec.SectDesc = SecStr
    End Sub

    Sub Set2GSA()
        Dim comList As New GSAComList
        comList.AddArray({"SET,PROP_SEC.1", CurSec.Ref, CurSec.Name, CurSec.Color, CurSec.Material, CurSec.SectDesc, "NO", "NA", 0, "NO_PROP,MOD_PROP", "BY", Aper, "BY", Iyyper, "BY", Izzper, "BY,1,BY,1,BY,1"})
        comList.RunCommand()
    End Sub

    'Sub Set2GSA_ss()
    '    GSAobj.SetSections({CurSec}, True)
    'End Sub
    Sub GetACR(LoadCase As String, EL_ref As Long)
        GetProperties()
        Dim aCom = New GSAComList
        Dim Ret As GsaResults()

        Ret = aCom.Get1DRet(ResHeader.REF_FORCE_EL1D, LoadCase, EL_ref)

        GetInfoFromName()


        Acr = Max(Acr, Abs(Ret(0).dynaResults(0) * 1000 / Area / Getfc(RCMat)))

    End Sub

    Sub GetProperties()
        Dim comList As New GSAComList
        comList.AddArray({"Get,SEC_PROP ", CurSec.Ref})
        Dim temparr = MF.CutStr(comList.RunCommand(), ",")

        Area = temparr(6)
    End Sub

    Sub GetInfoFromName()
        Dim i As Int16 = 1
        Dim strArr()
        Dim strArr1()

        strArr = MF.CutStr(GSAName, "_")

        SRatio = strArr(i) : i = i + 1
        RCMat = strArr(i) : i = i + 1
        RCDim = strArr(i) : i = i + 1
        SteelMat = strArr(i) : i = i + 1
        SteelShape = strArr(i) : i = i + 1
        SteelDim = strArr(i) : i = i + 1

        Dim j As Int16 = 0
        strArr1 = MF.CutStr(SteelDim, "x")
        If SteelShape = "I Section" Or "Box" Then
            Hs = strArr1(j) : j = j + 1
            Bs = strArr1(j) : j = j + 1
            tw = strArr1(j) : j = j + 1
            TF = strArr1(j) : j = j + 1
        ElseIf SteelShape = "Pipe" Then
            Ds = strArr(j) : j = j + 1
            t = strArr(j) : j = j + 1
        End If
    End Sub

    Function GetSDfc(RCMat As String, Ag As Double, Ast As Double)

        GetSDfc = (Getfc(Left(RCMat, 3)) * (Ag - Ast) + Getfs() * Ast) / Ag
    End Function
    Function Getfc(MatName As String) As Double
        Dim fc
        Select Case MatName
    'Unit:N/mm2
            Case "C15"
                fc = 7.2
            Case "C20"
                fc = 9.6
            Case "C25"
                fc = 11.9
            Case "C30"
                fc = 14.3
            Case "C35"
                fc = 16.7
            Case "C40"
                fc = 19.1
            Case "C45"
                fc = 21.1
            Case "C50"
                fc = 23.1
            Case "C55"
                fc = 25.3
            Case "C60"
                fc = 27.5
            Case "C65"
                fc = 29.7
            Case "C70"
                fc = 31.8
            Case "C75"
                fc = 33.8
            Case "C80"
                fc = 35.9
        End Select

        Return fc
    End Function
    Function Getfs()
        Dim tmax
        tmax = Math.Max(MF.MyMax(tf, tf90), MF.MyMax(tw, tw90, t))
        Dim fs = 250
        If tmax <= 50 Then fs = 265
        If tmax <= 35 Then fs = 295
        If tmax <= 16 Then fs = 310
        Return fs
    End Function
End Class



